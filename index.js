$(function() {
  var UsersJS = {
    init: function() {
      this.apiUrl = 'http://localhost:3000';
      this.bindEvents();
    },
    bindEvents: function() {
      $('.load-users').on('click', this.getUsers.bind(this));
    },
    getUsers: function() {
      var that = this;
      var url = this.apiUrl + '/users';
      
      $.get(url, function(data){
        var html = that.getUsersTemplate(data);
        $('.users-list').html(html);
      });
    },
    getUsersTemplate: function(data) {
      var that = this,
          template = '';
      
      data.forEach(function(item, index) {
          template = template + '<div class="user"><h2>' + item.name + '</h1><img src="' + that.apiUrl + item.path +'"/></div>' 
      });
      
      return template;
    }
  }
  UsersJS.init();
});